import React from "react";
import {Outlet} from 'react-router-dom'; 

const ModifyForm = () => {
    return (
        <>
            <form className="container-fluid p-4 border">
                <div className="row d-flex justify-content-center">
                    <div className="col-auto">
                        <div className="input-group">
                            <input
                                type="text"
                                className="form-control rounded-0 input-fonts p-2"
                                placeholder="FROM"
                                aria-label="From"
                                aria-describedby="basic-addon1"
                                value="Bangalore"
                            />
                        </div>
                    </div>
                    <div className="col-auto">
                        <div className="input-group rounded-0">
                            <input
                                type="text"
                                className="form-control rounded-0 input-fonts p-2"
                                placeholder="TO"
                                aria-label="To"
                                aria-describedby="basic-addon1"
                                value="Kumily"
                            />
                        </div>
                    </div>
                    <div className="col-auto">
                        <div className="input-group rounded-0">
                            <input
                                type="date"
                                className="form-control rounded-0 input-fonts p-2"
                                placeholder="TO"
                                aria-label="To"
                                aria-describedby="basic-addon1"
                                value="2022-09-08"
                            />
                        </div>
                    </div>
                    <div className="col-auto">
                        <button
                            type="submit"
                            className="btn btn-danger fw-bold rounded-0 input-fonts px-5 py-2"
                        >
                            Search
                        </button>
                    </div>
                </div>
            </form>
            <Outlet />
        </>
    );
}

export default ModifyForm; 