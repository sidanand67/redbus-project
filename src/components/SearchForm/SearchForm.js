import React from "react";
import './SearchForm.css'; 
import { Link } from "react-router-dom";

const SearchForm = () => {
    return (
        <form>
            <div className="row justify-content-center search-form align-items-center p-5 m-0">
                <div className="col-md-3 gx-0">
                    <div className="input-group">
                        <span
                            className="input-group-text rounded-0"
                            id="basic-addon1"
                        >
                            <i className="fa-solid fa-city"></i>
                        </span>
                        <div className="form-floating">
                            <input
                                type="text"
                                id="from-input"
                                className="form-control rounded-0 input-fonts"
                                placeholder="FROM"
                                aria-label="From"
                                aria-describedby="basic-addon1"
                            />
                            <label for="from-input">
                                FROM
                            </label>
                        </div>
                    </div>
                </div>

                <div className="col-md-3 gx-0">
                    <div className="input-group rounded-0">
                        <span
                            className="input-group-text rounded-0"
                            id="basic-addon1"
                        >
                            <i class="fa-solid fa-city"></i>
                        </span>
                        <div className="form-floating">
                            <input
                                type="text"
                                id="to-input"
                                className="form-control rounded-0 input-fonts" 
                                placeholder="TO"
                                aria-label="To"
                                aria-describedby="basic-addon1"
                            />
                            <label for="to-input">TO</label>
                        </div>
                    </div>
                </div>

                <div className="col-md-2 gx-0">
                    <div className="input-group rounded-0">
                        <span
                            className="input-group-text rounded-0"
                            id="basic-addon1"
                        >
                            <i class="fa-regular fa-calendar"></i>
                        </span>
                        <input
                            type="date"
                            className="form-control rounded-0 input-fonts p-3"
                            placeholder="TO"
                            aria-label="To"
                            aria-describedby="basic-addon1"
                        />
                    </div>
                </div>
                
                <div className="col-auto gx-0">
                    <Link
                        to="search"
                        className="btn search-btn text-white rounded-0 input-fonts p-3 px-4"
                    >
                        Search Buses
                    </Link>
                </div>
            </div>
        </form>
    );
}

export default SearchForm; 