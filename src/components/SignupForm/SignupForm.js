import React from "react";
import './SignupForm.css'; 

const SignupForm = () => {
    return (
        <>
            <div className="container-fluid d-flex">
                <div>
                    <img
                        className="img-pad img-fluid"
                        src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/LT_471_%28LTZ_1471%29_Arriva_London_New_Routemaster_%2819522859218%29.jpg/1200px-LT_471_%28LTZ_1471%29_Arriva_London_New_Routemaster_%2819522859218%29.jpg"
                        alt="image"
                    />
                </div>
                <form className="flex-fill align-self-center mx-5">
                    <p className="fw-bold text-danger fs-4">
                        Unlock the Smarter Way to Travel!
                    </p>
                    <div classNameName="mb-3">
                        <label
                            for="exampleInputEmail1"
                            className="form-label mb-1"
                        >
                            First Name
                        </label>
                        <input
                            type="email"
                            className="form-control mb-3 rounded-0"
                            id="exampleInputEmail1"
                            aria-describedby="emailHelp"
                        />
                    </div>
                    <div className="mb-3">
                        <label
                            for="exampleInputPassword1"
                            className="form-label mb-1"
                        >
                            Last Name
                        </label>
                        <input
                            type="text"
                            className="form-control mb-3 rounded-0 mb-4"
                            id="exampleInputPassword1"
                        />
                    </div>
                    <div className="mb-3">
                        <label
                            for="exampleInputPassword1"
                            className="form-label mb-1"
                        >
                            Email
                        </label>
                        <input
                            type="text"
                            className="form-control mb-3 rounded-0 mb-4"
                            id="exampleInputPassword1"
                        />
                    </div>
                    <div className="mb-3">
                        <label
                            for="exampleInputPassword1"
                            className="form-label mb-1"
                        >
                            Password
                        </label>
                        <input
                            type="password"
                            className="form-control mb-3 rounded-0 mb-4"
                            id="exampleInputPassword1"
                        />
                    </div>
                    <button type="submit" className="btn btn-danger rounded-0">
                        Sign Up
                    </button>
                </form>
            </div>
        </>
    );
};

export default SignupForm;
