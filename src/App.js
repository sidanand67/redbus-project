import React, { Component } from "react";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "@popperjs/core";

import { Routes, Route } from 'react-router-dom'; 
import Navbar from "./components/Navbar/Navbar";
import LandingPage from "./components/LandingPage/LandingPage";
import ResultsPage from "./components/ResultsPage/ResultsPage";
import SigninForm from "./components/SigninForm/SigninForm";
import SignupForm from "./components/SignupForm/SignupForm";
import SeatSelection from "./components/SeatSelection/SeatSelection";
import Footer from "./components/Footer/Footer";

class App extends Component {
    render(){
        return (
            <>
                <Navbar />
                <Routes>
                    <Route path="/" element={<LandingPage />} />
                    <Route path="search" element={<ResultsPage />} /> 
                    <Route path="/search/seats" element={<SeatSelection />} />
                    <Route path="signin" element={<SigninForm />} />
                    <Route path="signup" element={<SignupForm />} /> 
                </Routes>
                <Footer />
            </>
        )
    }
}

export default App;     