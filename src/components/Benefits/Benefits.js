import React from "react";
import './Benefits.css'; 

const Benefits = () => {
    return (
        <div className="container-fluid p-5 border border-2">
            <div className="row text-center">
                <div className="col">
                    <img
                        src="https://s1.rdbuz.com/web/images/home/promise.png"
                        className="mb-4"
                    />
                    <h2 className="mb-4 fw-bold fs-3">
                        WE PROMISE TO DELIVER
                    </h2>
                </div>
            </div>
            <div className="row shadow-lg">
                <div className="col bg-white py-3 px-5 border text-center">
                    <img
                        src="https://s2.rdbuz.com/web/images/home/benefits.png"
                        className="m-5"
                    />
                    <h6 className="fw-bold mb-3">UNMATCHED BENEFITS</h6>
                    <p className="promise-text">
                        We take care of your travel beyond ticketing by
                        providing you with innovative and unique benefits.
                    </p>
                </div>

                <div className="col bg-white py-3 px-5 border text-center">
                    <img
                        src="https://s1.rdbuz.com/web/images/home/customer_care.png"
                        className="m-5"
                    />
                    <h6 className="fw-bold mb-3">
                        SUPERIOR CUSTOMER SERVICE
                    </h6>
                    <p className="promise-text">
                        We put our experience and relationships to good use
                        and are available to solve your travel issues.
                    </p>
                </div>

                <div className="col bg-white py-3 px-5 border text-center">
                    <img
                        src="https://s1.rdbuz.com/web/images/home/lowest_Fare.png"
                        className="m-5"
                    />
                    <h6 className="fw-bold mb-3">LOWEST PRICES</h6>
                    <p className="promise-text">
                        We always give you the lowest price with the best
                        partner offers.
                    </p>
                </div>
            </div>
        </div>
    );
}

export default Benefits; 