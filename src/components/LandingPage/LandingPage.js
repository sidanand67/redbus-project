import React from "react";
import SearchForm from '../SearchForm/SearchForm'; 
import Offers from '../Offers/Offers'; 
import Benefits from "../Benefits/Benefits";
import GlobalPresence from "../GlobalPresence/GlobalPresence";
import Numbers from "../Numbers/Numbers";

const ResultsPage = () => {
    return (
        <>
            <SearchForm />
            <Offers />
            <Benefits />
            <GlobalPresence /> 
            <Numbers /> 
        </>
    );
};

export default ResultsPage;
