import React from "react";
import './SeatSelection.css'; 

const SeatSelection = (props) => {
    return (
        <div className="container-fluid bg-light">
            <div className="container">
                <div className="row justify-content-between">
                    <div className="col-md-5 mt-5">
                        <h5 className="fw-light fs-6">Upper Deck</h5>
                        <div className="row p-1">
                            <div className="col p-2 mb-5 border border-3">
                                <div className="row d-flex justify-content-between px-3">
                                    <div
                                        className="col-1 ms-5 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>
                                </div>

                                <div className="row d-flex justify-content-between px-3 mt-1">
                                    <div
                                        className="col-1 ms-5 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>
                                </div>

                                <div className="row d-flex justify-content-between px-3 mt-4">
                                    <div
                                        className="col-1 ms-5 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-5 mt-5">
                        <h5 className="fw-light fs-6">Lower Deck</h5>
                        <div className="row p-1">
                            <div className="col p-2 mb-5 border border-3">
                                <div className="row d-flex justify-content-between px-3">
                                    <div
                                        className="col-1 ms-5 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>
                                </div>

                                <div className="row d-flex justify-content-between px-3 mt-1">
                                    <div
                                        className="col-1 ms-5 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>
                                </div>

                                <div className="row d-flex justify-content-between px-3 mt-4">
                                    <div
                                        className="col-1 ms-5 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>

                                    <div
                                        className="col-1 border text-center bg-danger text-white"
                                        style={{ width: "70px" }}
                                    >
                                        S1
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    );
}

export default SeatSelection; 