import React from "react";

const Numbers = () => {
    return (
        <div className="container-fluid bg-white p-5 border border-2">
            <div className="row text-center">
                <p className="fs-2 fw-bold">THE NUMBERS ARE GROWING!</p>
            </div>
            <div className="row justify-content-between mt-5">
                <div className="col-sm-3 text-center">
                    <h6 className="fw-light">CUSTOMERS</h6>
                    <h1
                        className="text-danger"
                        style={{ fontSize: "50px" }}
                    >
                        36 M
                    </h1>
                    <p>
                        redBus is trusted by over 36 million happy customers
                        globally{" "}
                    </p>
                </div>

                <div className="col-sm-3 text-center">
                    <h6 className="fw-light">OPERATORS</h6>
                    <h1
                        className="text-danger"
                        style={{ fontSize: "50px" }}
                    >
                        3500
                    </h1>
                    <p>network of over 3500 bus operators worldwide</p>
                </div>

                <div className="col-sm-3 text-center">
                    <h6 className="fw-light">BUS TICKETS</h6>
                    <h1
                        className="text-danger"
                        style={{ fontSize: "50px" }}
                    >
                        220+ M
                    </h1>
                    <p>Over 220 million trips booked using redBus</p>
                </div>
            </div>
        </div>
    );
}

export default Numbers; 