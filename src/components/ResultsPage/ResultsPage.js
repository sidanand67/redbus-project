import React from "react";
import ModifyForm from "../ModifyForm/ModifyForm";
import SearchResultItem from "../SearchResultItem/SearchResultItem";

const ResultsPage = () => {
    return (    
        <div className="container-fluid bg-light">
            <ModifyForm /> 
            <SearchResultItem /> 
        </div>
    )
}

export default ResultsPage; 