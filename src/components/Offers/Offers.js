import './Offers.css'; 

const Offers = () => {
    return (
        <div className="container-fluid bg-light p-5">
            <div className="row d-flex justify-content-center">
                <div className="col shadow text-center bg-white img-thumbnail p-3 offer-card">
                    <p className='offer-text'>Save up to Rs 300 on bus tickets</p>
                    <img
                        src="https://st.redbus.in/Images/RHSS/2022/hero/274x147.png"
                        alt="offer"
                        className='my-1'
                    />
                    <p className='offer-text'>Use code FIRST</p>
                </div>
                <div className="col shadow text-center bg-white img-thumbnail p-3 offer-card">
                    <p className='offer-text'>Save up to Rs 300 on bus tickets</p>
                    <img
                        src="https://st.redbus.in/images/INDOFFER/RB200/ravganapathi/tile2-274x148.png"
                        alt="offer"
                        className='my-1'
                        />
                    <p className='offer-text'>Use code SUPERHIT</p>
                </div>
                <div className="col shadow text-center bg-white img-thumbnail p-3 offer-card">
                    <p className='offer-text'>Save up to Rs 200 in Delhi, West India &amp; North India</p>
                    <img
                        src="https://st.redbus.in/Images/INDOFFER/RB200/274x148.png"
                        alt="offer"
                        className='my-1'
                    />
                    <p className='offer-text'>Use code RB200</p>
                </div>
            </div>
        </div>
    );
}

export default Offers;  