import React from "react";
import './SigninForm.css'; 

const SigninForm = () => {
    return (
        <>
            <div className="container-fluid d-flex">
                <div>
                    <img
                        className="img-signin img-fluid bg-secondary"
                        src="http://listaka.com/wp-content/uploads/2015/08/To-travel-far-enough-to-meet-yourself.jpg"
                        alt="image"
                    />
                </div>
                <form className="flex-fill align-self-center mx-5">
                    <p className="fw-bold text-danger fs-4">
                        Sign in to avail exciting discounts and cashbacks!!
                    </p>

                    <div className="mb-3">
                        <label
                            for="exampleInputPassword1"
                            className="form-label mb-1"
                        >
                            Email
                        </label>
                        <input
                            type="text"
                            className="form-control mb-3 rounded-0 mb-4"
                            id="exampleInputPassword1"
                        />
                    </div>
                    <div className="mb-3">
                        <label
                            for="exampleInputPassword1"
                            className="form-label mb-1"
                        >
                            Password
                        </label>
                        <input
                            type="password"
                            className="form-control mb-3 rounded-0 mb-4"
                            id="exampleInputPassword1"
                        />
                    </div>
                    <button type="submit" className="btn btn-danger rounded-0">
                        Sign In
                    </button>
                </form>
            </div>
        </>
    );
}

export default SigninForm; 