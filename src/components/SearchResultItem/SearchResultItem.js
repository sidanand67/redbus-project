import React, { Component } from "react";
import './SearchResultItem.css'; 
import busData from './busdata.json'; 
import { Link } from "react-router-dom";

class SearchResultItem extends Component{
    constructor(props){
        super(props); 
        this.state = {
            busData: []
        }
    }

    componentDidMount () {
        this.setState({
            busData: busData
        }); 
    }

    render(){
        let renderBusData = this.state.busData.map(bus => {
            return (
                <div className="container bg-white border border-2 p-4 my-4" key={bus.name}> 
                    <div className="row m-1 mb-2 align-items-center">
                        <div className="col fw-bolder fs-3">
                            {bus.name}{" "}
                            <span
                                className="bg-warning img-thumbnail px-2 py-1 text-dark align-middle"
                                style={{ fontSize: "13px" }}
                            >
                                <i className="fa-solid fa-star me-1"></i>
                                {bus.ratings.rate}
                            </span>
                        </div>
                    </div>

                    <div className="row m-1 align-items-center">
                        <div className="col fw-light fs-6 text-dark">
                            SOURCE:{" "}
                            <span className="fs-5 fw-bolder">
                                {bus.departureCity}
                            </span>
                        </div>
                        <div className="col fw-light fs-6 text-dark">
                            DESTINATION:{" "}
                            <span className="fs-5 fw-bolder">
                                {bus.arrivalCity}
                            </span>
                        </div>
                    </div>

                    <div className="row m-1 align-items-center">
                        <div className="col fw-light fs-6 text-dark">
                            DEPARTURE:{" "}
                            <span
                                className="fs-4 fw-bold"
                                style={{ letterSpacing: "2px" }}
                            >
                                {bus.departureTime}
                            </span>
                        </div>
                        <div
                            className="col fw-light fs-6 text-dark"
                            style={{ letterSpacing: "2px" }}
                        >
                            ARRIVAL:{" "}
                            <span className="fs-4 fw-bold">
                                {bus.arrivalTime}
                            </span>
                        </div>
                        <div className="col fw-light fs-6 text-dark">
                            DURATION:{" "}
                            <span className="fs-5 fw-bolder">
                                {bus.duration}
                            </span>
                        </div>
                    </div>

                    <div className="row m-1 align-items-center">
                        <div className="col fw-light fs-6 text-dark">
                            FARE:
                            <span className="fs-3 fw-bold ms-2">
                                <i className="fa-solid fa-indian-rupee-sign fs-5"></i>{" "}
                                {bus.fare}
                            </span>
                        </div>
                        <div className="col fw-light fs-6 text-dark">
                            SEATS AVAILABLE:{" "}
                            <span className="fs-5 fw-bolder">
                                {bus.seatsAvailable}
                            </span>
                        </div>
                        <div className="col d-flex justify-content-center">
                            <Link
                                to="seats"
                                className="btn btn-danger rounded-0"
                            >
                                VIEW SEATS
                            </Link>
                        </div>
                    </div>
                </div>
            );
        })
        return (
            <div className="container">
                {renderBusData}
            </div>
        )
    }
}

export default SearchResultItem; 