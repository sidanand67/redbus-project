import React from "react";
import { Outlet } from "react-router-dom";
import "./Breadcrumb.css"; 

const Breadcrumb = () =>{
    return (
        <div className="container-fluid py-1 bg-light">
            <div className="container">
                <nav className="breadcrumb my-1" aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item breadcrumb-font-size fw-bold">
                            Home
                        </li>
                        <li
                            className="breadcrumb-item breadcrumb-font-size"
                            aria-current="page"
                        >
                            Bus Tickets
                        </li>
                        <li
                            className="breadcrumb-item breadcrumb-font-size"
                            aria-current="page"
                        >
                            Bangalore (Bengaluru) Bus
                        </li>
                        <li
                            className="breadcrumb-item breadcrumb-font-size active text-danger"
                            aria-current="page"
                        >
                            Bangalore (Bengaluru) To Kumily Bus
                        </li>
                    </ol>
                </nav>
            </div>
            <Outlet />
        </div>
    );
}

export default Breadcrumb; 