import React from "react";
import { Outlet } from "react-router-dom";
import "./Navbar.css";
import {Link} from 'react-router-dom'; 

const Navbar = () => {
    return (
        <>
            <div className="container-fluid bg-color">
                <nav className="navbar navbar-expand-lg py-0">
                    <div className="container-fluid">
                        <Link to="/" className="navbar-brand" href="#">
                            <img
                                className="brand-logo"
                                src="https://s3.rdbuz.com/web/images/home/sgp/r_logo.png"
                                alt="brand"
                            />
                        </Link>
                        <button
                            className="navbar-toggler"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#navbarNavDropdown"
                            aria-controls="navbarNavDropdown"
                            aria-expanded="false"
                            aria-label="Toggle navigation"
                        >
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div
                            className="collapse navbar-collapse"
                            id="navbarNavDropdown"
                        >
                            <ul className="navbar-nav ms-auto">
                                <li className="nav-item">
                                    <a
                                        className="nav-link nav-item-text text-white"
                                        aria-current="page"
                                        href="https://www.redbus.in/info/redcare"
                                    >
                                        Help
                                    </a>
                                </li>

                                <li className="nav-item dropdown">
                                    <a
                                        className="nav-link dropdown-toggle nav-item-text text-white"
                                        href="#"
                                        role="button"
                                        data-bs-toggle="dropdown"
                                        aria-expanded="false"
                                    >
                                        Manage Booking
                                    </a>
                                    <ul className="dropdown-menu">
                                        <li>
                                            <a
                                                className="dropdown-item nav-item-text"
                                                href="#"
                                            >
                                                Cancel
                                            </a>
                                        </li>
                                        <li>
                                            <a
                                                className="dropdown-item nav-item-text"
                                                href="#"
                                            >
                                                Change Travel Date
                                            </a>
                                        </li>
                                        <li>
                                            <a
                                                className="dropdown-item nav-item-text"
                                                href="#"
                                            >
                                                Show My Ticket
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li className="nav-item dropdown">
                                    <a
                                        className="nav-link dropdown-toggle nav-item-text text-white"
                                        href="#"
                                        role="button"
                                        data-bs-toggle="dropdown"
                                        aria-expanded="false"
                                    >
                                        <i class="fa-regular fa-user fs-5 me-1 align-middle"></i> My Account 
                                    </a>
                                    <ul className="dropdown-menu">
                                        <li>
                                            <a
                                                className="dropdown-item nav-item-text shadow-none"
                                                href="#"
                                            >
                                                Sign In / Sign Up
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <Outlet />
        </>
    );
};

export default Navbar;
