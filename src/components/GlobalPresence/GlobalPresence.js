import React from "react";
import './GlobalPresence.css'; 
import "/node_modules/flag-icons/css/flag-icons.min.css";

const GlobalPresence = () => {
    return (
        <div className="container-fluid bg-light p-5">
            <div className="row">
                <p className="fs-3 fw-bolder text-center">
                    OUR GLOBAL PRESENCE
                </p>
            </div>

            <div className="row my-4 text-center justify-content-between">
                <div className="col-sm-3 justify-content-center">
                    <span class="fi fi-co flag-icon mb-2"></span>
                    <p className="fw-bold country-name">COLOMBIA</p>
                </div>

                <div className="col-sm-3 justify-content-center">
                    <span class="fi fi-in flag-icon mb-2"></span>
                    <p className="fw-bold country-name">INDIA</p>
                </div>

                <div className="col-sm-3 justify-content-center">
                    <span class="fi fi-id flag-icon mb-2"></span>
                    <p className="fw-bold country-name">INDONESIA</p>
                </div>
            </div>

            <div className="row mt-5 text-center justify-content-between">
                <div className="col-sm-3 justify-content-center">
                    <span class="fi fi-my flag-icon mb-2"></span>
                    <p className="fw-bold country-name">MALAYSIA</p>
                </div>

                <div className="col-sm-3 justify-content-center">
                    <span class="fi fi-pe flag-icon mb-2"></span>
                    <p className="fw-bold country-name">PERU</p>
                </div>

                <div className="col-sm-3 justify-content-center">
                    <span class="fi fi-sg flag-icon mb-2"></span>
                    <p className="fw-bold country-name">SINGAPORE</p>
                </div>
            </div>
        </div>
    );
}

export default GlobalPresence; 