import React from "react";
import './Footer.css'; 

const Footer = () => {
    return (
        <div className="container-fluid footer-color p-5">
            <div className="row d-flex justify-content-between">
                <div className="col-auto text-white">
                    <h6 className="text-secondary fw-bold mb-3">
                        About redBus
                    </h6>
                    <ul className="footer-list">
                        <li>About Us</li>
                        <li>Contact Us</li>
                        <li>Mobile Version</li>
                        <li>redBus on Mobile</li>
                        <li>Sitemap</li>
                    </ul>
                </div>

                <div className="col-auto text-white">
                    <h6 className="text-secondary fw-bold mb-3">Info</h6>
                    <ul className="footer-list">
                        <li>T &amp; C</li>
                        <li>Privacy Policy</li>
                        <li>FAQ</li>
                        <li>Blog</li>
                        <li>Bus Operator Registration</li>
                        <li>Agent Registration</li>
                        <li>Insurance Partner</li>
                    </ul>
                </div>

                <div className="col-auto text-white">
                    <h6 className="text-secondary fw-bold mb-3">
                        Global Sites
                    </h6>
                    <ul className="footer-list">
                        <li>India</li>
                        <li>Singapore</li>
                        <li>Malaysia</li>
                        <li>Indonesia</li>
                        <li>Peru</li>
                        <li>Colombia</li>
                    </ul>
                </div>

                <div className="col-auto text-white">
                    <h6 className="text-secondary fw-bold mb-3">
                        Our Partners
                    </h6>
                    <ul className="footer-list">
                        <li>Goibibo</li>
                        <li>Makemytrip</li>
                    </ul>
                </div>

                <div className="col-md-3 text-white">
                    <img
                        src="https://s3.rdbuz.com/web/images/home/sgp/r_logo.png"
                        className="mb-3"
                    />
                    <p className="fw-light lh-base">
                        redBus is the world's largest online bus ticket
                        booking service trusted by over 25 million happy
                        customers globally. redBus offers bus ticket booking
                        through its website,iOS and Android mobile apps for
                        all major routes.
                        <span className="d-block">
                            <i className="fa-brands fa-facebook fs-2 mt-3"></i>
                            <i class="fa-brands fa-twitter fs-2 mt-3 mx-3"></i>
                        </span>
                    </p>
                    <p className="fw-light">&copy; 2022 ibibogroup All rights reserved</p>
                </div>
            </div>
        </div>
    );
}

export default Footer; 